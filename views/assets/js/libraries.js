// Fungsi Daftar Hari
function FListDay(day=''){
    let list = ['minggu','senin','selasa','rabu','kamis','jum\'at','sabtu'];
    if(day === ''){
        return list;
    }else{
        return list[day];
    }
}

// Fungsi Daftar Bulan
function FListMonth(month=''){
    let list = ['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'];
    if(month === ''){
        return list;
    }else{
        return list[month];
    }
}

// Fungsi Render Jam
function FGetRealTimeClock(timestamp, countinterval=1000){
    let maincontent = document.getElementById('main-content'),
        timer       = document.getElementById('timer'),
        tmstmps     = timestamp,
        localTime   = +Date.now(),
        timeDiff    = tmstmps - localTime;

    return setInterval(function(){
        let time        = new Date(+Date.now() + timeDiff),
            day         = FListDay(),
            month       = FListMonth(),
            dy          = time.getDay(),
            d           = time.getDate(),
            m           = time.getMonth(),
            y           = time.getFullYear(),
            h           = time.getHours(),
            min         = time.getMinutes(),
            s           = time.getSeconds(),
            hh          = (h < 10 ? '0'+h : h),
            mm          = (min < 10 ? '0'+min : min),
            ss          = (s < 10 ? '0'+s : s),
            now         = day[dy] +', '+ d +' '+ month[m] +' '+ y +' | '+ hh +':'+ mm +':'+ ss;
        timer.innerHTML = now+' WIB';
    }, countinterval);
}

// Fungsi Bloodhound
function FSetBloodhound(url,value,token){
    return new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace(value),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: url,
            wildcard: '%QUERY',
            prepare: function (query, settings) {
                settings.url = settings.url + '/' + query
                settings.headers = {
                    "Authorization": "Bearer " + token
                };

                return settings;
            },
            cache: false
        },
    });
}

// Fungsi Bloodhound Local
function FSetBloodhoundLocal(source){
    return new Bloodhound({
        initialize: false,
        local : source,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('info'),
        identify: function(obj){
            return obj.text;
        }
    });
}

// Fungsi Bloodhound Search
function FSetBloodhoundSearch(source){
    return new Bloodhound({
        local : source,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('info','text'),
    });
}

// Fungsi RegexSource Typeahead
function FSetRegex(strings){
    return function findMatches(q, cb) {
        let matches, substringRegex,
            substrRegex = new RegExp(q, 'i');

        matches = [];
        $.each(strings, function(i, str) {
            if (substrRegex.test(str.info)) {
                matches.push(str);
            }
        });
        cb(matches);
    };
}

// Fungsi Render DataTables
function FGenerateDataTable(url,token,columns=[],rowcallback=false,afterinit=false,plength=20){
    return {
        initComplete: afterinit,
        pageLength: plength,
        "ajax": {
            "url": url,
            "dataType": 'json',
            "type": "GET",
            "beforeSend": function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        },
        "columns": columns,
        "oLanguage": {
            "sSearch": "Cari ",
            "oPaginate": {
                "sPrevious": "Sebelumnnya",
                "sNext": "Selanjutnya",
            },
            "sEmptyTable": "Belum ada Data yang tersedia",
            "sLengthMenu": "Tampilkan  _MENU_  baris",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ entri",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 entri",
            "infoFiltered": "",
            "loadingRecords": "&nbsp;",
            "sProcessing": "Sedang memuat data, mohon menunggu ..."
        },
        "fnRowCallback": rowcallback,
        "order": [],
        "bLengthChange": false,
        language: {
            buttons: {
                colvis: '<i class="ti-view-grid"></i>'
            },
        },
        processing: true,
    }
}

// Fungsi DTB Filter
function FSetDTBFilter(table,listcolumn=[]){
    table.columns(listcolumn).every(function(indexcol){
        let setcolumn = this,
            select = $('<select class="rounded-0 pd-2-f text-capitalize"><option value="" class="text-capitalize">-- SEMUA --</option></select>')
                .appendTo($(setcolumn.footer()).empty())
                .on('change', function(){
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    setcolumn.search(val ? '^'+val+'$' : '',true,false).draw();
                });

        setcolumn.data().unique().sort().each(function(d,j){
            select.append('<option value="'+d+'" class="text-capitalize">'+d+'</option>')
        });

        if(indexcol === 3){
            setcolumn.search('belum bayar').draw();
        }

    });
}

function FSetDTBFilterDraw(table,listcolumn=[]){
    table.on('draw', function(){
        table.columns().indexes().each(function(idx){
            var select = $(table.column(idx).footer()).find('select');
            if(select.val() === ''){
                select.empty().append('<option value="">-- SEMUA --</option>');
                table.column(idx, {search:'applied'}).data().unique().sort().each(function(d,j){
                    select.append( '<option value="'+d+'">'+d+'</option>' );
                });
            }
        });
    })
}

// Fungsi untuk ambil JSON dari URL
function FGetJSONFromURL(url='',token='',method='GET',async=false,sender=''){
    let getreturn = Object;
    if(url.length > 0){
        let req;
        if(window.XMLHttpRequest){
            req = new XMLHttpRequest();
        }else{
            req = new ActiveXObject('Microsoft.XMLHTTP');
        }
        req.open(method, url, async);
        if(method === 'POST'){
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        }
        if(token.length > 0){
            req.setRequestHeader('Authorization','Bearer ' + token);
        }

        if(sender === ''){
            req.send();
        }else{
            req.send(sender);
        }
        if(req.status === 200){
            getreturn = req.response;
        }else{
            FAlertAlwaysFailed('Ambil Manifest Gagal');
        }
    }

    return JSON.parse(getreturn);
}

// Fungsi Huruf Kapital
function FStringCapitalize(string){
    let tostring = string.toString();
    return tostring.toLowerCase().replace(/^(.)|(\s|\-)(.)/g,
        function(c) {
            return c.toUpperCase();
        });
}

// Fungsi REQUEST POST/GET AJAX
function FRequestAJAX(url, param, ispost, onbefore={}, onsuccess={}, onerror={}){
    let method = ispost ? "POST" : "GET";
    return $.ajax({
        type: method,
        url: url,
        beforeSend: onbefore,
        data: param,
        success: onsuccess,
        error: onerror
    });
}

// Fungsi Sweetalert2
function FAlert(seticon,settitle='Ooppss..',settext,setonafterclose={},settimer=2000){
    return Swal.fire({
        icon: seticon,
        title: settitle,
        text: settext,
        timer: settimer,
        confirmButtonColor: '#0168fa',
        timerProgressBar: true,
        onAfterClose: setonafterclose,
    })
}

// Fungsi Alert Gagal
function FAlertAlwaysFailed(message){
    return Swal.fire({
        icon: 'error',
        title: '<strong>BCApps Galat</strong>',
        text: message,
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
    })
}

// Fungsi Loading Bar
function FLoadingBar(message){
    return Swal.fire({
        title: '<img src="/assets/img/spinner-roulette.gif" width="75" />',
        html: '<strong>'+message+'</strong>',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        CancelButtonText: "No"
    })
}

// Fungsi Deteksi Inputan Angka
function FKeyInputOnlyNumeric(event){
    let getKeyCode = (event.which) ? event.which : event.keyCode;
    if(getKeyCode > 31 && (getKeyCode < 48 || getKeyCode > 57) ){
        return false;
    }else{
        return getKeyCode !== 13;
    }
}

// Fungsi Deteksi Inputan Huruf
function FKeyInputOnlyAlpha(event){
    let getKeyCode = (event.which) ? event.which : event.keyCode;
    if(getKeyCode > 47 && getKeyCode < 58){
        event.preventDefault();
    }
}

// Fungsi Inputan Rupiah
function FSetInputNumericToRupiah(angka, prefix){
    let number_string   = angka.toString().replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi),
        separator;

    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix === undefined ? rupiah : (rupiah ? 'Rp' + rupiah : '') + ",00";
}

// Fungsi Inputan Rupiah ke Angka
function FSetInputRupiahToNumeric(angka){
    return parseInt(angka.toString().replace(/,.*|[^0-9]/g, ''), 10);
}

// Fungsi Inputan To Rupiah
function FSetInputToRupiah(angka){
    let tempangka = angka.split('.'),
        tempnominal = tempangka[0].replace(/\,/g,'.');

    return "Rp" + tempnominal + ",00";
}

// Fungsi Input Focust ke Element Berikutnya
function FFocusToNextElement() {
    let focussableElements = 'button.btn-primary, input[type=email]:not([disabled]), input[type=text]:not([disabled]), select:not([disabled]), [tabindex]:not([disabled]):not([tabindex="-1"])';
    if (document.activeElement && document.activeElement.form) {
        let focussable = Array.prototype.filter.call(document.activeElement.form.querySelectorAll(focussableElements),
            function(element) {
                return element.offsetWidth > 0 || element.offsetHeight > 0 || element === document.activeElement
            });
        let index = focussable.indexOf(document.activeElement);
        focussable[index + 1].focus();
    }
}

// Fungsi Acak
function FSetRandomString(list,length,isupper=true) {
    let result           = '',
        character        = list.trim(),
        charactersLength = character.length;
    for ( var i = 0; i < length; i++ ) {
       result += character.charAt(Math.floor(Math.random() * charactersLength));
    }
    return isupper ? result.toUpperCase() : result;
 }

// Fungsi Transform Text
(function($) {
    $.fn.extend({
        upperFirstAll: function() {
            $(this).keyup(function(event) {
                var box         = event.target;
                var txt         = $(this).val();
                var start       = box.selectionStart;
                var end         = box.selectionEnd;
                $(this).val(txt.toLowerCase().replace(/^(.)|(\s|\-)(.)/g,
                    function(c) {
                        return c.toUpperCase();
                    }));
                box.setSelectionRange(start, end);
            });
            return this;
        },

        upperFirst: function() {
            $(this).keyup(function(event) {
                var box = event.target;
                var txt = $(this).val();
                var start = box.selectionStart;
                var end = box.selectionEnd;

                $(this).val(txt.toLowerCase().replace(/^(.)/g,
                    function(c) {
                        return c.toUpperCase();
                    }));
                box.setSelectionRange(start, end);
            });
            return this;
        },

        lowerCase: function() {
            $(this).keyup(function(event) {
                var box = event.target;
                var txt = $(this).val();
                var start = box.selectionStart;
                var end = box.selectionEnd;

                $(this).val(txt.toLowerCase());
                box.setSelectionRange(start, end);
            });
            return this;
        },

        upperCase: function() {
            $(this).keyup(function(event) {
                var box = event.target;
                var txt = $(this).val();
                var start = box.selectionStart;
                var end = box.selectionEnd;

                $(this).val(txt.toUpperCase());
                box.setSelectionRange(start, end);
            });
            return this;
        }

    });
})(jQuery);
