$(function(){

  'use strict'

  $('[data-toggle="tooltip"]').tooltip()

  const asideBody = new PerfectScrollbar('#GU_PrimaryNavigation', {
    suppressScrollX: true
  });

  $('.nav-aside .with-sub').on('click', '.nav-link', function(e){
    e.preventDefault();

    $(this).parent().siblings().removeClass('show');
    $(this).parent().toggleClass('show');

    asideBody.update()
  })
})
