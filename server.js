const   path    = require('path'),
        express = require('express'),
        fs      = require('fs'),
        bodyParser  = require('body-parser'),
        xlsx        = require('xlsx'),
        moment      = require('moment'),
        formidable  = require('formidable'),
        mv          = require('mv'),
        exceljs     = require('exceljs'),
        storage     = require('sqlite3').verbose(),
        filestorage = './storage/rvd.sqlite',
        storespath  = __dirname + '/storage/json/store.json',
        courierspath= __dirname + '/storage/json/courier.json',
        app         = express(),
        dbstorage   = new storage.Database(filestorage),
        jsonstores  = require(storespath),
        jsoncouriers   = require(courierspath);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
 
app.get('/orders/',(req, res) => {
    let SQL = 'SELECT * FROM orders ORDER BY is_scanned DESC';

    dbstorage.all(SQL, [], (err,rows) => {
      let output = {};

      if(err){
          output = {
              status: err.message,
              query: SQL
          }
      }else{
        let getjsonstore    =  fs.readFileSync(storespath,'utf-8'),
            getjsoncourier  =  fs.readFileSync(courierspath,'utf-8'),
            parsejsonstore  = JSON.parse(getjsonstore),
            parsejsoncourier= JSON.parse(getjsoncourier),
            storelist = {}, courierlist = {};

        parsejsonstore.forEach((item) => {
            storelist[item.storeid] = item.storename
        });

        parsejsoncourier.forEach((item) => {
            courierlist[item.courierid] = item.couriername
        });

        rows.forEach((itemrow) => {
            itemrow['storeword'] = storelist[itemrow.storeid]
            itemrow['courierword'] = courierlist[itemrow.courierid]
        })

          output = {
              draw: 0,
              recordsTotal: rows.length,
              recordsFiltered: rows.length,
              data: rows
          }
      }
      res.json(output);
    });
});

app.post('/orders/add',(req, res) => {
    let SQL         = 'INSERT INTO orders(product_raw,courierid,customer_name,city,storeid,shipping_fee,invoice_number,shipping_date,order_created_at,barcode_number,phone_number,created_at)',
        paramquery  = req.body.paraminput,
        datecreated = moment().format('YYYY-MM-DD kk:mm:ss');

    SQL += ' VALUES("'+paramquery['descproduct']+'","'+paramquery['courier']+'","'+paramquery['name']+'","'+paramquery['city']+'","'+paramquery['ecommerce']+'","'+paramquery['fee']+'","'+paramquery['invoice']+'","'+paramquery['dateshipping']+'","'+paramquery['dateorder']+'","'+paramquery['barcode']+'","'+paramquery['phone']+'","'+datecreated+'")'
    dbstorage.run(SQL, [], (err) => {
        let output = {};

        if(err){
            output = {
                status: false,message: err.message + ' - ' + SQL
            }
        }else{
            output = {
                status: true,message: 'ok'
            }
        }
        res.json(output);
    })
});

app.post('/orders/update',(req, res) => {
    let SQL         = 'UPDATE orders SET product_raw = ?, courierid = ?, customer_name = ?, city = ?, storeid = ?, shipping_fee = ?, invoice_number = ?, shipping_date = ?, order_created_at = ?, barcode_number = ?, phone_number = ? WHERE orderid = ?',
        paramquery  = req.body.paraminput,
        datecreated = moment().format('YYYY-MM-DD kk:mm:ss');

    dbstorage.run(SQL, [paramquery['descproduct'],paramquery['courier'],paramquery['name'],paramquery['city'],paramquery['ecommerce'],paramquery['fee'],paramquery['invoice'],paramquery['dateshipping'],paramquery['dateorder'],paramquery['barcode'],paramquery['phone'],paramquery['byorder']], (err) => {
        let output = {};

        if(err){
            output = {
                status: false,message: err.message + ' - ' + SQL
            }
        }else{
            output = {
                status: true,message: 'ok'
            }
        }
        res.json(output);
    })
});

app.get('/stores/',(req, res) => {
    let output  = {
        draw: 0,
        recordsTotal: jsonstores.length,
        recordsFiltered: jsonstores.length,
        data: jsonstores
    }

    res.json(output)
});

app.get('/couriers/',(req, res) => {
    let output  = {
        draw: 0,
        recordsTotal: jsoncouriers.length,
        recordsFiltered: jsoncouriers.length,
        data: jsoncouriers
    }

    res.json(output)
});

app.post('/:typemodule/add',(req, res) => {
    let getparamurl = req.params.typemodule,
        output = {};

    if(getparamurl === 'stores' || getparamurl === 'couriers'){
        if(getparamurl === 'stores'){
            jsonstores.push({
                "storeid" : req.body.paramid,
                "storename": req.body.paramname
            })
            fs.writeFileSync(storespath,JSON.stringify(jsonstores))
            output = {
                status: true,
                message: 'data sukses ditambahkan'
            }
        }else{
            jsoncouriers.push({
                "courierid" : req.body.paramid,
                "couriername": req.body.paramname
            })
            fs.writeFileSync(courierspath,JSON.stringify(jsoncouriers))
            output = {
                status: true,
                message: 'data sukses ditambahkan'
            }
        }
    }else{
        output = {
            status: false,
            message: 'module tidak sesuai'
        }
    }

    res.json(output)
});

app.post('/:typemodule/update',(req, res) => {
    let getparamurl = req.params.typemodule,
        output = {};

    if(getparamurl === 'stores' || getparamurl === 'couriers'){
        if(getparamurl === 'stores'){
            let isfound = jsonstores.find((item) => {
                return item.storeid === req.body.paramid
            }), dataupdated = {}, targetindex = 0;

            if(isfound){
                dataupdated = {
                    "storeid" : req.body.paramid,
                    "storename": req.body.paramname
                }
                targetindex = jsonstores.indexOf(isfound)
                jsonstores.splice(targetindex,1,dataupdated)
                fs.writeFileSync(storespath,JSON.stringify(jsonstores))
                output = {
                    status: true,
                    message: 'data sukses diperbarui'
                }
            }else{
                output = {
                    status: false,
                    message: 'data gagal diperbarui'
                }
            }
        }else{
            let isfound = jsoncouriers.find((item) => {
                return item.courierid === req.body.paramid
            }), dataupdated = {}, targetindex = 0;

            if(isfound){
                dataupdated = {
                    "courierid" : req.body.paramid,
                    "couriername": req.body.paramname
                }
                targetindex = jsoncouriers.indexOf(isfound)
                jsoncouriers.splice(targetindex,1,dataupdated)
                fs.writeFileSync(courierspath,JSON.stringify(jsoncouriers))
                output = {
                    status: true,
                    message: 'data sukses diperbarui'
                }
            }else{
                output = {
                    status: false,
                    message: 'data gagal diperbarui'
                }
            }
        }
    }else{
        output = {
            status: false,
            message: 'module tidak sesuai'
        }
    }

    res.json(output)
});

app.post('/:typemodule/delete',(req, res) => {
    let getparamurl = req.params.typemodule,
        output = {};

    if(getparamurl === 'stores' || getparamurl === 'couriers'){
        if(getparamurl === 'stores'){
            let isfound = jsonstores.find((item) => {
                return item.storeid === req.body.paramid
            }), targetindex = 0;

            if(isfound){
                targetindex = jsonstores.indexOf(isfound)
                jsonstores.splice(targetindex,1)
                fs.writeFileSync(storespath,JSON.stringify(jsonstores))
                output = {
                    status: true,
                    message: 'data sukses dihapus'
                }
            }else{
                output = {
                    status: false,
                    message: 'data gagal dihapus'
                }
            }
        }else{
            let isfound = jsoncouriers.find((item) => {
                return item.courierid === req.body.paramid
            }), targetindex = 0;

            if(isfound){
                targetindex = jsoncouriers.indexOf(isfound)
                jsoncouriers.splice(targetindex,1)
                fs.writeFileSync(courierspath,JSON.stringify(jsoncouriers))
                output = {
                    status: true,
                    message: 'data sukses dihapus'
                }
            }else{
                output = {
                    status: false,
                    message: 'data gagal dihapus'
                }
            }
        }
    }else{
        output = {
            status: false,
            message: 'module tidak sesuai'
        }
    }

    res.json(output)
});

app.get('/last-query/',(req, res) => {
  let SQL = 'SELECT created_at FROM orders ORDER BY is_scanned DESC';

  dbstorage.all(SQL, [], (err,rows) => {
    let output = {};

    if(err){
        output = {
            status: err.message,
            query: SQL
        }
    }else{
        output = {
            draw: 0,
            recordsTotal: rows.length,
            recordsFiltered: rows.length,
            data: rows
        }
    }

    res.json(output);
  });
});

app.post('/scan-barcode',(req, res) => {
  let SQL   = 'SELECT * FROM orders WHERE barcode_number = ? AND barcode_number <> "null"',
      paramquery = req.body.code;

    dbstorage.all(SQL, [paramquery], (err,rows) => {
        let count = rows.length,
            output = {};

        if(err){
            output = {
                status: false,
                message: err.message
            }
        }else{
            if(count > 0){
                output = {
                    status: true,
                    match: true,
                    message: 'query ok with results',
                    paramq: paramquery
                }
            }else{
                output = {
                    status: true,
                    match: false,
                    message: paramquery,
                    paramq: paramquery
                }
            }
        }

        res.json(output)
    });
});

app.get('/export-excel/',(req, res) => {
    let SQL = 'SELECT product_raw,courierid,customer_name,city,storeid,shipping_fee FROM orders WHERE is_scanned = 1 ORDER BY customer_name ASC',
        fileexcelname = 'laporan_harian_',
        getjsonstore =  fs.readFileSync(storespath,'utf-8'),
        parsejsonstore = JSON.parse(getjsonstore),
        storelist = {}

        parsejsonstore.forEach((item) => {
            storelist[item.storeid] = item.storename
        });

    dbstorage.all(SQL, [], (err,rows) => {
        let output = {},
            xls = [],
            workbook = new exceljs.Workbook(),
            worksheet= workbook.addWorksheet('laporan_harian');

        if(err){
            output = {
                status: false,
                message: err.message
            }
        }else{
            output = {
                status: true,
                message: 'ok'
            }
        }

        worksheet.columns = [
            {header: 'No',key:'idindex',width:5},
            {header: 'Orderan',key:'product_raw',width:50},
            {header: 'Ekspedisi',key:'courierid',width:20},
            {header: 'Kepada',key:'customer_name',width:20},
            {header: 'Kota',key:'city',width:30},
            {header: 'Toko',key:'storeid',width:20},
        ];

        worksheet.mergeCells('A1','F3')
        worksheet.getCell('A1').value   = 'DAFTAR KIRIMAN ' + moment().format('DD MMMM YYYY')
        worksheet.getCell('A1').font = {bold: true,size:'20px'}
        worksheet.getCell('A1').alignment = {
            vertical: 'middle',
            horizontal: 'center'
        }
        worksheet.getCell('A1').fill    = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FFFF00' }
        }
        worksheet.getRow(5).values  = ['No','Orderan','Ekspedisi','Kepada','Kota','Toko']
        worksheet.getRow(5).font = {bold: true}

        rows.forEach((e,index) => {
            xls.push({
                idindex: index+1,
                product_raw: e.product_raw,
                courierid: e.courierid,
                customer_name: e.customer_name,
                city: e.city,
                storeid: storelist[e.storeid],
            })
        })
        worksheet.addRows(xls)
        workbook.xlsx.writeFile(__dirname + '/report/laporan_harian_' + moment().format('DD-MMM-YYYY_HH-mm-ss') + ').xlsx')
        res.json(output)
    });
});

app.post('/set-is-scanned',(req, res) => {
  let SQL   = 'UPDATE orders SET is_scanned = 1 WHERE barcode_number = ?',
      paramquery = req.body.code;

    dbstorage.run(SQL, [paramquery], (err) => {
        let output  = {};

        if(err){
            output = {
                status: false,
                message: err.message
            }
        }else{
            output = {
                status: true,
                match: false,
                message: 'query ok',
            }
        }

        res.json(output)
    });
});

app.post('/upload-excel',(req, res) => {
    let storageexcel = __dirname + '/storage/excel/',
        form = new formidable.IncomingForm();

    form.parse(req, function(err, fields, files){
        let oldpath = files.excelfile.path,
            newpath = storageexcel + 'template.xlsx';

        mv(oldpath, newpath, function(err){
            if(err){
                res.json({status: false,is_uploaded: false,message: err})
            }else{
                let workbook  = xlsx.readFile(storageexcel + 'template.xlsx'),
                  sheetname = workbook.SheetNames,
                  exceldata = xlsx.utils.sheet_to_json(workbook.Sheets['Sheet1']),
                  SQLInsert = 'INSERT INTO orders(product_raw,courierid,customer_name,city,storeid,shipping_fee,invoice_number,shipping_date,order_created_at,barcode_number,created_at) VALUES(',
                  SQLInsertValue = [];

                exceldata.forEach((rows) => {
                    let tempRow = [];
                    for(const [key,val] of Object.entries(rows)){
                      tempRow.push(val);
                    }

                    SQLInsertValue.push(SQLInsert + "'" + tempRow.join("','") + "','"+ moment().format('YYYY-MM-DD kk:mm:ss') +"')")
                })

                SQLInsertValue.forEach((command) => {
                    dbstorage.run(command, [], (err) => {
                        let output = {};

                        if(err){
                            output = {
                                status: false,is_uploaded: false,message: err.message + ' - ' + command
                            }
                        }else{
                            output = {
                                status: true,is_uploaded: true,message: 'ok'
                            }
                        }
                        res.end(JSON.stringify(output));
                    })
                })
            }
        });
    });
});

app.get('/reset/',(req, res) => {
    let output = {};

    fs.copyFile(__dirname + '/storage/rvd-empty.sqlite', __dirname + '/storage/rvd.sqlite', (err) => {
        if(err){
            output = {
                status: false,message: err.message + ' - ' + SQL
            }
        }else{
            output = {
                status: true,message: 'ok'
            }
        }
        res.json(output);
    })
});


app.listen(8000, () => {
  console.log('Server is running at port 8000');
});