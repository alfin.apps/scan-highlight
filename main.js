const   {app,BrowserWindow} = require('electron')

let createWindow = () => {
    const mainWindow = new BrowserWindow({
        width: 1200,
        height: 800,
        webPreferences: {
          nodeIntegration: true
        },
    }),
        passbyquery = {
            baseurl: 'http://localhost:8000'
        };

    mainWindow.webContents.openDevTools()
    mainWindow.setMenuBarVisibility(false)
    mainWindow.setResizable(false)
    mainWindow.loadFile(__dirname + '/views/index.html')
}

app.whenReady().then(createWindow)
app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        app.quit()
    }
})

app.on('activate', () => {
    if(BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})